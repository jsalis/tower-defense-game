/**
 * Class:       HeadsUpDisplay
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        August 6, 2014
 */

HeadsUpDisplay.prototype.constructor = HeadsUpDisplay;

function HeadsUpDisplay(renderer)
{
	this.scene = new THREE.Scene();

	// Create camera and add it to the scene
	var camRatio = window.innerWidth / window.innerHeight;
    var hudWidth = 50;
    var hudHeight = hudWidth / camRatio;
    this.camera = new THREE.OrthographicCamera(-hudWidth, hudWidth, hudHeight, -hudHeight, 1, hudWidth);
    this.camera.position.z = 2;
    this.scene.add(this.camera);

    var textPad = 3;
    var textSize = 1.5;
    var labelCount = 0;
    var textMaterial = new THREE.MeshBasicMaterial({ color: 0x008A7E });

    this.addLabel = function(label)
    {
    	var textGeometry = new THREE.TextGeometry(label, {
			font: 'optimer',
			weight: 'bold',
			size: textSize,
			height: 0,
		});
		var textMesh = new THREE.Mesh(textGeometry, textMaterial);
		textMesh.position.x = -hudWidth + textPad;
		textMesh.position.y = -hudHeight + (textPad * (labelCount + 1)) + (textSize * labelCount);
		this.scene.add(textMesh);

		labelCount ++;
    };

    this.render = function()
	{
		renderer.autoClear = false;
		renderer.render(this.scene, this.camera);
		renderer.autoClear = true;
	};
}
