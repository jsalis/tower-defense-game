/**
 * Class:       GameModel
 * Author:      Nathan Hilliard
 * Purpose:     
 * 
 * Date:        October 28th, 2014
 */

// Declare our constructor
GameModel.prototype.constructor = GameModel;

// Some constants for the mode.
GameModel.mode = {
	NONE: 0,
	ADD_SHOCKWAVE: 1,
	ADD_CANNON: 2,
	ADD_BLOCKER: 3
};

GameModel.state = {
    PLAYING: 1,
    GAME_OVER: 2
};

GameModel.audio = [];

GameModel.audio['error'] = new Audio('_audio/beep.wav');
GameModel.audio['error'].volume = 0.4

GameModel.audio['growth'] = new Audio('_audio/growth.wav');
GameModel.audio['growth'].volume = 0.4;;

GameModel.audio['spawn'] = new Audio('_audio/spawn.ogg');
GameModel.audio['spawn'].volume = 0.5;

GameModel.audio['death'] = new Audio('_audio/death.wav');
GameModel.audio['death'].volume = 0.5;

GameModel.audio['damage'] = new Audio('_audio/damage.mp3');
GameModel.audio['damage'].volume = 0.5;

GameModel.audio['pew'] = new Audio('_audio/pew.mp3');
GameModel.audio['pew'].volume = 0.1;

GameModel.audio['gameover'] = new Audio('_audio/gameover.wav');
GameModel.audio['gameover'].volume = 0.5;

GameModel.audio['killchain'] = new Audio('_audio/killchain.mp3');
GameModel.audio['killchain'].volume = 0.5;
GameModel.audio['killchain'].loop = true;

function GameModel(scene, camera, renderer)
{
	// A reference to the scene so we can add and remove objects.
	this.scene = scene;
	this.scene.camera = camera;

    // GameModel.audio['killchain'].play();

	// Initialize credits used to purchase towers and weapons
	this.scene.credits = 30;

	// Initialize difficulty scalar
	this.scene.wave = 0;

    // Initialize the state of the game.
    this.scene.state = GameModel.state.PLAYING;

	// Create HUD
	var headsUpDisplay = new HeadsUpDisplay(renderer);

	// Create map
	var mapController = new MapController(scene);

	// Create pointer for selecting cells on the map
    this.pointer = new Pointer(scene, camera, mapController.getMap().data);

    // Add the floor geometry that the pointer will interact with
    this.pointer.addTarget(mapController.getFloor());

    // The game mode. E.g. Adding towers.
    this.mode = GameModel.mode.NONE;

    // An an event that will fire on click when they've clicked on the map.
    this.pointer.addOnClickEvent((function() {

    	// If its game over then just return
    	if (this.scene.state == GameModel.state.GAME_OVER)
    		return;

    	if (this.mode == GameModel.mode.ADD_BLOCKER)
    	{
    		if (this.scene.credits >= Blocker.cost)
        	{
        		var s = this.pointer.getSelection();
		        if (this.scene.add(new Blocker(s.row, s.col)))
		        {
		        	this.scene.credits -= Blocker.cost;

		        	GameModel.audio['growth'].currentTime = 0;
		        	GameModel.audio['growth'].play();
		        }
        	}
        	else
        		GameModel.audio['error'].play();
    	}
    	else if (this.mode == GameModel.mode.ADD_SHOCKWAVE)
    	{ 
    		if (this.scene.credits >= ShockwaveTower.cost)
        	{
        		var s = this.pointer.getSelection();
		        if (this.scene.add(new ShockwaveTower(s.row, s.col)))
		        {
		        	this.scene.credits -= ShockwaveTower.cost;

		        	GameModel.audio['growth'].currentTime = 0;
		       		GameModel.audio['growth'].play();
		        }
        	}
        	else
        		GameModel.audio['error'].play();
    	}
    	else if (this.mode == GameModel.mode.ADD_CANNON)
    	{ 
    		if (this.scene.credits >= CannonTower.cost)
        	{
        		var s = this.pointer.getSelection();
		        if (this.scene.add(new CannonTower(s.row, s.col)))
		        {
		        	this.scene.credits -= CannonTower.cost;

		        	GameModel.audio['growth'].currentTime = 0;
		       		GameModel.audio['growth'].play();
		        }
        	}
        	else
        		GameModel.audio['error'].play();
    	}
    }).bind(this), false, true);

    this.pointer.addOnClickEvent((function() {
        this.mode = GameModel.mode.NONE;
        this.pointer.setRangeVisible(false);
    }).bind(this), true, null);

    this.scene.updatePaths = function(cellIndex)
	{
		for (var i = 0; i < this.children.length; i++)
	    {
	    	var child = this.children[i];
	    	if (child instanceof Spawner || child instanceof Monster)
	    	{
	    		if (!child.updatePath(mapController.getMap().data, cellIndex))
	    		{
	    			return false;
	    		}
	    	}
	    }
	    return true;
	};

    // Override the add function for the scene so the map data is updated
    this.scene.add = function(object)
    {
    	THREE.Scene.prototype.add.call(this, object);

    	if (object instanceof Tower || object instanceof Blocker)
    	{
    		var pos = object.getCellRowCol();
    		mapController.setMapElement(pos.row, pos.col, Map.GAME_OBJECT);

    		// Let all pathfinding GameObjects know that the map data has changed
    		// If not all paths can be updated then the object must be removed
    		if (!this.updatePaths(object.getCellIndex()))
    		{
    			this.remove(object);
    			GameModel.audio['error'].play();
    			return false;
    		}
    	}
    	return true;
    };

    // Override the remove function for the scene so the map data is updated
    this.scene.remove = function(object)
    {
    	THREE.Scene.prototype.remove.call(this, object);

    	if (object instanceof Tower || object instanceof Blocker)
    	{
    		var pos = object.getCellRowCol();
    		mapController.setMapElement(pos.row, pos.col, Map.FLOOR);
    	}
    };

    // Create a base on the bottom row
    var pos = mapController.getMap().findEdgeCell(false, true);
    this.scene.add(new Base(pos.row, pos.col, 20));

    // Create a spawner on the top row
    var pos = mapController.getMap().findEdgeCell(true, true);
    var spawner = new Spawner(pos.row, pos.col);
    this.scene.add(spawner);
    spawner.findTarget();
    spawner.setPath(mapController.getMap().data);

    // Create a spawner on the right column
    var pos = mapController.getMap().findEdgeCell(true, false);
    var spawner = new Spawner(pos.row, pos.col);
    this.scene.add(spawner);
    spawner.findTarget();
    spawner.setPath(mapController.getMap().data);

    // Create a spawner on the left column
    var pos = mapController.getMap().findEdgeCell(false, false);
    var spawner = new Spawner(pos.row, pos.col);
    this.scene.add(spawner);
    spawner.findTarget();
    spawner.setPath(mapController.getMap().data);

    // GameModel run function
	this.run = function()
	{
        // Render the scene
        renderer.render(scene, camera);

	    // Update pointer
	    this.pointer.enabled = (this.mode != GameModel.mode.NONE);
	    this.pointer.run();

	    // Update all game objects in the scene
	    for (var i = 0; i < this.scene.children.length; i++)
	    {
            if (this.scene.children[i] instanceof Base)
            {
                if (this.scene.children[i].health == 0 && 
                	this.scene.state != GameModel.state.GAME_OVER)
                {
                    GameModel.audio['killchain'].pause();
                    GameModel.audio['gameover'].play();
                    this.scene.state = GameModel.state.GAME_OVER;
                }
            }

	    	// Run each game object
	    	if (this.scene.children[i] instanceof GameObject)
	    	{
	    		this.scene.children[i].run();
	    	}
	    }

        // Render the HUD
        headsUpDisplay.render();
	};
}
