/**
 * Class:       MapController
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        July 27, 2014
 */

MapController.prototype.constructor = MapController;

function MapController(scene)
{
	MapView.loadResources(Map.SIZE, Map.SCALE);

	var map = new Map();
    map.initialize(Map.DISTRIBUTION);
	map.generate(Map.PASS_COUNT, Map.SMOOTH_PASS_COUNT);

    var mapView = new MapView(scene, map.data, 0, 0, map.seed);

    this.getMap = function()
    {
        return map;
    };

    this.getFloor = function()
    {
        return mapView.getFloor();
    };

    this.getCellPosition = function(row, col)
    {
    	return map.getCellPosition(row, col);
    };

    this.setMapElement = function(row, col, val)
    {
        map.setElement(row, col, val);
    };
}
