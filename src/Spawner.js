/**
 * Class:       Spawner
 * Author:      Nathan Hilliard
 * Purpose:     
 * 
 * Date:        October 28th, 2014
 */

// Inherit the GameObject class
Spawner.prototype = Object.create(GameObject.prototype);

// Declare our constructor
Spawner.prototype.constructor = Spawner;

Spawner.pathMaterial = new THREE.LineBasicMaterial({ color: 0x383838, linewidth: 1.5 });

function Spawner(row, col)
{
	// Call parent constructor
	GameObject.call(this, row, col);

	// Set the max and min spawn rate
	this.maxRate = 660;
	this.minRate = 360;

	// Set the cooldown to the min spawn rate
	this.cooldown = this.minRate;

	this.target = null;
	this.path = [];
	this.pathMesh = null;

	this.init();
}

// Initialize the geometry for the Spawner object
Spawner.prototype.init = function()
{
	var color = 0xFF5858;
	var size = Map.SCALE;
	var geometry = new THREE.BoxGeometry(size, size, size);
	var material = new THREE.MeshLambertMaterial({ color: color });
    var mesh = new THREE.Mesh(geometry, material);

    mesh.scale.divideScalar(GameObject.outlineScale);
    mesh.position.y = geometry.parameters.height / 2;
    this.add(mesh);

    // Create light
    var light = new THREE.PointLight(color, 4, 6);
    light.position.y = geometry.parameters.height;
    this.add(light);

    // Create outline mesh
    var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
    outline.position.y = mesh.position.y;
    this.add(outline);
};

Spawner.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// If its game over then just return
    if (this.parent.state == GameModel.state.GAME_OVER)
        return;

	if (this.cooldown > 0)
	{
		this.cooldown --;

		if (this.cooldown <= 0 && this.path.length > 0)
		{
			// Reset the cooldown to a random number between the min and max spawn rate
			this.cooldown = Math.round(Math.random() * (this.maxRate - this.minRate)) + this.minRate;
			this.spawn();
		}
	}
};

Spawner.prototype.spawn = function()
{
	GameModel.audio['spawn'].currentTime = 0;
	GameModel.audio['spawn'].play();

	var pos = this.getCellRowCol();

	// Return a new monster positioned at the spawner
	var enemy = new Monster(pos.row, pos.col);
	enemy.target.row = this.target.row;
	enemy.target.col = this.target.col;
	enemy.path = this.path.slice(0);
	this.parent.add(enemy);
};

Spawner.prototype.findTarget = function()
{
	// Get a list of the other objects in the scene
	var siblings = this.parent.children;
	var baseList = [];

	for (var i = 0; i < siblings.length; i++)
	{
		if (siblings[i] instanceof Base)
		{
			baseList.push(siblings[i]);
		}
	}

	if (baseList.length > 0)
	{
		// Choose a random base from the list and set it as the target
		var index = Math.round(Math.random() * (baseList.length - 1));
		this.target = baseList[index].getCellRowCol();
		return true;
	}
	else
	{
		this.target = null;
		return false;
	}
};

Spawner.prototype.setPath = function(data)
{
	// Clear the path
	this.path = [];

	if (this.target !== null)
	{
		var start = this.getCellRowCol();
		this.path = Generator.findPath(data, start, this.target);
		this.updatePathMesh();
	}
};

/**
 * Updates the path if the specified cell index is on the current path.
 * 
 * @param  {Array}  data       The 2D array of map data.
 * @param  {Number} cellIndex  The index of the cell to search for.
 * @return {Boolean}           False if the path is blocked, true otherwise.
 */
Spawner.prototype.updatePath = function(data, cellIndex)
{
	// Check if the specified cell index is on the path.
	for (var i = 0; i < this.path.length; i++)
	{
		// If so, the path must be updated.
		if (this.path[i] == cellIndex)
		{
			var start = this.getCellRowCol();
			var newPath = Generator.findPath(data, start, this.target);

			if (newPath.length > 0)
			{
				this.path = newPath;
				this.updatePathMesh();
				return true;
			}
			else
				return false;
		}
	}
	return true;
};

Spawner.prototype.updatePathMesh = function()
{
	// Remove mesh for the path
	if (this.pathMesh !== null)
	{
		this.pathMesh.geometry.dispose();
		this.parent.remove(this.pathMesh);
	}

	// Create new mesh for the path
	var geometry = new THREE.Geometry();
	for (var i = 0; i < this.path.length; i++)
	{
		var rc = Map.getRowCol(this.path[i]);
		var pos = Map.getCellPosition(rc.row, rc.col);
		geometry.vertices.push(new THREE.Vector3(pos.x, 0, pos.z));
	}
    this.pathMesh = new THREE.Line(geometry, Spawner.pathMaterial);
    this.parent.add(this.pathMesh);
};
