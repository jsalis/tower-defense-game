
$(document).ready(function() {

    var frustumFar = (Map.SIZE * Map.SCALE * 4);
    var fov = window.innerWidth / window.innerHeight;

    var renderer = initRenderer('webGL');
    var stats = initStats('stats');

    window.addEventListener('resize', onWindowResize, false);

    // Create scene
    var scene = new THREE.Scene();

    // Create camera and add it to the main scene
    var camera = new THREE.PerspectiveCamera(45, fov, 1, frustumFar);
    scene.add(camera);

    // Create game model
    var gameModel = new GameModel(scene, camera, renderer);

    // Create input manager
    var inputManager = new InputManager();
    inputManager.mapKey(action.MOVE_FORWARD, 87);
    inputManager.mapKey(action.MOVE_BACKWARD, 83);
    inputManager.mapKey(action.MOVE_LEFT, 65);
    inputManager.mapKey(action.MOVE_RIGHT, 68);
    inputManager.mapKey(action.TURN_LEFT, 37);
    inputManager.mapKey(action.TURN_RIGHT, 39);
    inputManager.mapKey(action.ONE, 49);
    inputManager.mapKey(action.TWO, 50);
    inputManager.mapKey(action.THREE, 51);
    inputManager.mapKey(action.FOUR, 52);

    // Create skybox
    var segments = 40;
    var radius = Map.SIZE * Map.SCALE * 1.5;
    var map = THREE.ImageUtils.loadTexture('img/skybox.jpg');
    var skyGeometry = new THREE.SphereGeometry(radius, segments, segments);
    var skyMaterial = new THREE.MeshBasicMaterial({ side: THREE.BackSide, map: map });
    var skyMesh = new THREE.Mesh(skyGeometry, skyMaterial);
    skyMesh.position.y = radius * 0.64;
    scene.add(skyMesh);
    
    // Create hemisphere light
    var h_Light = new THREE.HemisphereLight(0xffffff, 0xffffff, 1);
    scene.add(h_Light);

    // Create directional light
    var d_light = new THREE.DirectionalLight(0xffffff, 0.5);
    d_light.position.set(0, 20, 20);
    d_light.target.position.set(0, 0, 0);
    scene.add(d_light);

    // Create a controller for the camera
    var cameraController = new THREE.OrbitControls(camera);
    cameraController.minDistance = Map.SCALE * 4;
    cameraController.maxDistance = Map.SIZE * Map.SCALE;
    // cameraController.maxPolarAngle = Math.PI / 2.2;
    cameraController.target = new THREE.Vector3(0, 0, 0);

    // Initialize camera position
    camera.position.x = 0;
    camera.position.y = cameraController.maxDistance / 2;
    camera.position.z = cameraController.maxDistance / 2;
    cameraController.update();

    initGamePanel();
    run();

    function initRenderer(containerId)
    {
        // Get container
        container = document.getElementById(containerId);

        // Create renderer and add it to the container
        var renderer = new THREE.WebGLRenderer({ antialias: false });
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);

        renderer.shadowMapEnabled = false;
        renderer.shadowMapSoft = false;

        renderer.shadowMapDarkness = 0.5;

        renderer.shadowCameraNear = 1;
        renderer.shadowCameraFar = frustumFar;
        renderer.shadowCameraFov = fov;
        
        renderer.shadowMapWidth = 1024;
        renderer.shadowMapHeight = 1024;

        return renderer;
    }

    function initStats(containerId)
    {
        var stats = new Stats();
        stats.setMode(0); // 0: fps, 1: ms

        // Align top-left
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';

        $('#' + containerId).append(stats.domElement);

        return stats;
    }

    function initGamePanel()
    {
        this.controls = new function() {
            // When the element fires we need to modify the pointer so it knows that
            // onClick for any valid cell to place a tower to place an arrowTower and deduct points.
            this.shockwaveTower = function() {
                gameModel.mode = GameModel.mode.ADD_SHOCKWAVE;
                gameModel.pointer.setRangeVisible(true);
                gameModel.pointer.setRange(ShockwaveTower.range);
            };

            this.cannonTower = function() {
                gameModel.mode = GameModel.mode.ADD_CANNON;
                gameModel.pointer.setRangeVisible(true);
                gameModel.pointer.setRange(CannonTower.range);
            };

            this.blocker = function() {
                gameModel.mode = GameModel.mode.ADD_BLOCKER;
                gameModel.pointer.setRangeVisible(false);
            };

            this.credits = gameModel.scene.credits;
            //this.wave = gameModel.scene.wave;
        }

        // Initialize a new GUI class.
        this.gui = new dat.GUI({ width: 300 });

        // Add some folders.
        var towerFolder = this.gui.addFolder('Towers');
        var toolsFolder = this.gui.addFolder('Tools');
        var stateFolder = this.gui.addFolder('Game State');

        // Inject <img> tag into the name of the UI control so we can have an icon.
        // It's even possible to inject CSS through use of the 'style' attribute of HTML tags.
        var shockwaveTowerIcon = '<img src="img/shockwave.png" width=20 height=20 />';
        var shockwaveTowerName = 'Shockwave (' + ShockwaveTower.cost + ')';

        var shockwaveTowerButton = shockwaveTowerIcon + shockwaveTowerName;

        // Do it again for the cannon tower control element.
        var cannonTowerIcon = '<img src="img/cannonballs.png" width=20 height=20 />';
        var cannonTowerName = 'Cannon (' + CannonTower.cost + ')';

        var cannonTowerButton = cannonTowerIcon + cannonTowerName;

        // And one more time for the blocker.
        var blockerIcon = '<img src="img/blocker.png" width=20 height=14 />';
        var blockerName = 'Blocker (' + Blocker.cost + ')';

        var blockerButton = blockerIcon + blockerName;

        // Add the controls to the towers folder with modified name properties.
        towerFolder.add(this.controls, 'shockwaveTower').name(shockwaveTowerButton);
        towerFolder.add(this.controls, 'cannonTower').name(cannonTowerButton);
        towerFolder.open();

        toolsFolder.add(this.controls, 'blocker').name(blockerButton);
        toolsFolder.open();

        stateFolder.add(this.controls, 'credits').name('Credits').listen();
        // stateFolder.add(this.controls, 'wave').name('Wave').listen();

        stateFolder.open();
    }

    function onWindowResize()
    {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function run()
    {
        var forwardKey = inputManager.down(action.MOVE_FORWARD);
        var backwardKey = inputManager.down(action.MOVE_BACKWARD);
        var leftKey = inputManager.down(action.MOVE_LEFT);
        var rightKey = inputManager.down(action.MOVE_RIGHT);

        // Update game model
        gameModel.run();

        // Update performance stats
        stats.update();

        // Update the score and wave based on the value in the Game Model.
        this.controls.credits = gameModel.scene.credits;
        //this.controls.wave = gameModel.scene.wave;
        
        // Request another frame
        requestAnimationFrame(run);
    }
});
