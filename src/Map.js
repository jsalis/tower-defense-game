/**
 * @class 			Map
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	Stores terrain data for a single map.
 */

Map.SCALE = 2;
Map.SIZE = 32;

Map.EMPTY = 0;
Map.FLOOR = 1;
Map.GAME_OBJECT = 3;

Map.DISTRIBUTION = 0.48;

Map.PASS_COUNT = 6;
Map.BORN_THRESHOLD = 5;
Map.SURVIVE_THRESHOLD = 3;
Map.MOORE_SIZE = 1;
Map.SMOOTH_PASS_COUNT = 2;
Map.SMOOTH_THRESHOLD = 5;

Map.prototype.constructor = Map;

function Map()
{
	this.seed = Generator.getSeed();
	this.data = null;
}

Map.prototype.initialize = function(distribution)
{
	this.data = Generator.getRandomData(Map.SIZE, distribution, { seed: this.seed });
};

Map.prototype.generate = function(passCount, smoothCount)
{
	for (var i = 0; i < passCount; i++)
	{
		this.data = Generator.applyCellAutomata(this.data, Map.BORN_THRESHOLD, Map.SURVIVE_THRESHOLD);
	}

	for (var i = 0; i < smoothCount; i++)
	{
		this.data = Generator.applyCellAutomata(this.data, Map.SMOOTH_THRESHOLD, Map.SMOOTH_THRESHOLD);
	}

	Generator.isolateMaxRegion(this.data, Map.FLOOR, Map.EMPTY);
};

/**
 * Finds a random cell on a spedified edge of the map. There are four possible edges,
 * the top and bottom row, and the left and right column. Any cell on the far edge has
 * an equal probability of being chosen.
 * 
 * @param  {Boolean} dir        The direction of the search. True for positive, false for negative.
 * @param  {Boolean} usingRows  Whether to search by rows or columns of the map data.
 * @return {Object}             An object containing the row and col. Null if not found.
 */
Map.prototype.findEdgeCell = function(dir, usingRows)
{
	var start = (dir) ? 0 : Map.SIZE - 1;
	var d = (dir) ? 1 : -1;

	for (var i = start; i >= 0 && i < Map.SIZE; i += d)
	{
		var list = [];
		for (var j = 0; j < Map.SIZE; j++)
		{
			if (this.data[i][j] == Map.FLOOR && usingRows)
			{
				list.push({ row: i, col: j });
			}
			else if (this.data[j][i] == Map.FLOOR && !usingRows)
			{
				list.push({ row: j, col: i });
			}
		}
		if (list.length > 0)
		{
			var index = Math.round(Math.random() * (list.length - 1));
			return list[index];
		}
	}
	return null;
};

Map.getCellPosition = function(row, col)
{
	var length = Map.SIZE * Map.SCALE;
	var z = (row * Map.SCALE) + (Map.SCALE / 2) - (length / 2);
	var x = (col * Map.SCALE) + (Map.SCALE / 2) - (length / 2);
	return { z : z, x : x };
};

Map.getCellRowCol = function(z, x)
{
	var length = Map.SIZE * Map.SCALE;
	var row = Math.floor((z + (length / 2)) / Map.SCALE);
	var col = Math.floor((x + (length / 2)) / Map.SCALE);
	return { row : row, col : col };
};

Map.prototype.getRandomCellRowCol = function(val)
{
	return Generator.getRandomPosition(this.data, val);
};

Map.prototype.inRange = function(row, col)
{
	return (row >= 0 && col >= 0 && row < this.data.length && col < this.data[0].length);
};

Map.prototype.getIndex = function(data, row, col)
{
	if (this.inRange(row, col))
	{
		return (row * Map.SIZE) + col;
	}
	else return -1;
};

Map.getRowCol = function(i)
{
	var r = Math.floor(i / Map.SIZE);
	var c = i % Map.SIZE;
	return { row: r, col: c };
};

Map.prototype.getElement = function(row, col)
{
	return (this.inRange(row, col)) ? this.data[pos.row][pos.col] : null;
};

Map.prototype.setElement = function(row, col, val)
{
	if (this.inRange(row, col))
	{
		this.data[row][col] = val;
	}
};

Map.prototype.print = function()
{
	if (this.data != null)
	{
		Generator.print(this.data);
	}
};
