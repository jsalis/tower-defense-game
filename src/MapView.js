/**
 * @class 			MapView
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	Builds geometry to represent map data and controls the adding and removing 
 *               	of map objects to the scene.
 */

MapView = (function() {

	/**
	 * @constant {Number} 	The Y scale of the map walls in relation to the X Z scale.
	 */
	var WALL_SCALE = 0.5;

	/**
	 * @constant {Number} 	The offset amount for the base vertices of the wall geometry.
	 */
	var WALL_OFFSET = -0.25;

	/**
	 * @constant {Number} 	The decimal precision to use when comparing vertex positions.
	 */
	var PRECISION = 1 / Math.pow(10, 3);

	/**
	 * @constant {Boolean} 	The amount to shift the entire map mesh along the Y axis.
	 */
	var Y_SHIFT = -0.05;

	/**
	 * @var {Boolean} 	Whether resources needs to be loaded using MapView.loadResources().
	 */
	var needsLoad = true;

	/**
	 * Class Constructor
	 * 
	 * @param {THREE.Object3D} 	scene 	The container for all view objects.
	 * @param {Array} 			data   	A 2D array of map data.
	 * @param {Number} 			z     	The center Z position.
	 * @param {Number} 			x     	The center X position.
	 * @param {String} 			seed  	The seed to be used for random generation.
	 */
	function MapView(scene, data, z, x, seed)
	{
		if (needsLoad)
		{
			console.error("MapView resources not loaded.");
		}

		this._scene = scene;
		this._seed = seed;

		// FLOOR
		this._floorMesh = createFloorMesh(data, z, x);
		this._scene.add(this._floorMesh);

		// WALL
		this._wallMesh = createWallMesh(data, this._seed);
		this._wallMesh.position.set(x, -MapView.blockHeight + Y_SHIFT, z);
		this._wallMesh.scale.set(MapView.scale, MapView.scale, MapView.scale);
		this._wallMesh.updateMatrix();
		this._scene.add(this._wallMesh);
	}

	/**
	 * Creates a new floor mesh using a given set of map data.
	 * 
	 * @param  {Array} 		data 	A 2D array of map data.
	 * @param  {Number} 	z   	The center Z position.
	 * @param  {Number} 	x   	The center X position.
	 * @return {THREE.Mesh}     	The floor mesh.
	 */
	function createFloorMesh(data, z, x)
	{
		var geometry = new PartitionGeometry(MapView.mapLength, MapView.size, data);
		var mesh = new THREE.Mesh(geometry, MapView.floorMaterial);
		mesh.receiveShadow = true;
		mesh.rotation.x = Math.PI / -2;
		mesh.position.x = x;
		mesh.position.z = z;
		mesh.position.y = Y_SHIFT;
		return mesh;
	}

	/**
	 * Creates a new wall mesh using a given set of map data.
	 * 
	 * @param  {Array} 		data 	A 2D array of map data.
	 * @param  {String} 	seed 	The seed for random generation.
	 */
	function createWallMesh(data, seed)
	{
		var mapLength = data.length - 1;

		var wallGeometry = new THREE.Geometry();

		for (var i = 0; i < mapLength; i++)
		{
			for (var j = 0; j < mapLength; j++)
			{
				var block = createBlock(data, i, j);
				if (block == null) continue;

				var bz = i + (1 / 2) - (mapLength / 2);
				var bx = j + (1 / 2) - (mapLength / 2);

				block.position.z = bz;
				block.position.x = bx;
				block.position.y = WALL_SCALE / 2;
				block.updateMatrix();
				wallGeometry.merge(block.geometry, block.matrix);
			}
		}

		wallGeometry.mergeVertices();
		wallGeometry.computeVertexNormals();

		var wallMesh = new THREE.Mesh(wallGeometry, MapView.blockFaceMaterial);
		wallMesh.receiveShadow = true;
		wallMesh.updateMatrix();
		wallMesh.updateMatrixWorld();

		extendBaseVertices(wallMesh.geometry, wallMesh.matrixWorld);

		return wallMesh;
	}

	/**
	 * Creates a block mesh for a single element in the map data. The geometry depends on the value 
	 * of the element and its immediate neighbors.
	 * 
	 * @param  {Array} 		data 	A 2D array of map data.
	 * @param  {Integer} 	row 	The row position of the block.
	 * @param  {Integer} 	col 	The column position of the block.
	 * @return {THREE.Mesh}     	The generated block mesh.
	 */
	function createBlock(data, row, col)
	{
		var center = data[row][col] != Map.FLOOR;

		if (!center) return null;

		var north = data[row - 1][col] != Map.FLOOR;
		var south = data[row + 1][col] != Map.FLOOR;
		var west = data[row][col - 1] != Map.FLOOR;
		var east = data[row][col + 1] != Map.FLOOR;

		if (north && south && west && east) return null;

		// Set flags for the four possible tip configurations
		var nn = north && !west && !south && !east;
		var ss = !north && !west && south && !east;
		var ww = !north && west && !south && !east;
		var ee = !north && !west && !south && east;

		var tip = nn || ss || ww || ee;

		// Set flags for the four possible corner configurations
		var nw = north && west && !south && !east;
		var ne = north && east && !south && !west;
		var sw = south && west && !north && !east;
		var se = south && east && !north && !west;

		var corner = nw || ne || sw || se;

		var block;

		if (tip)
		{
			block = new THREE.Mesh(MapView.tipBlockGeometry);
		}
		else if (corner)
		{
			block = new THREE.Mesh(MapView.cornerBlockGeometry);
		}
		else
		{
			var blockGeometry = new THREE.Geometry();

			if (!north)
			{
				var side = new THREE.Mesh(MapView.sideBlockGeometry);
				side.rotation.y = Math.PI;
				side.updateMatrix();
				blockGeometry.merge(side.geometry, side.matrix);
			}
			if (!east)
			{
				var side = new THREE.Mesh(MapView.sideBlockGeometry);
				side.rotation.y = Math.PI / 2;
				side.updateMatrix();
				blockGeometry.merge(side.geometry, side.matrix);
			}
			if (!west)
			{
				var side = new THREE.Mesh(MapView.sideBlockGeometry);
				side.rotation.y = Math.PI / -2;
				side.updateMatrix();
				blockGeometry.merge(side.geometry, side.matrix);
			}
			if (!south)
			{
				var side = new THREE.Mesh(MapView.sideBlockGeometry);
				blockGeometry.merge(side.geometry);
			}
			block = new THREE.Mesh(blockGeometry);
		}

		// Set material index for faces
		for (var n = 0; n < block.geometry.faces.length; n++)
		{
			block.geometry.faces[n].materialIndex = data[row][col];
		}

		// Set block rotation
		if (ss || se)
		{
			block.rotation.y = Math.PI;
		}
		else if (ww || sw)
		{
			block.rotation.y = Math.PI / 2;
		}
		else if (ee || ne)
		{
			block.rotation.y = Math.PI / -2;
		}
		return block;
	}

	/**
	 * Transforms base vertices of a geometry in the direction of their normals.
	 * 
	 * @param  {THREE.Geometry} geometry    	The geometry to apply the transformation.
	 * @param  {THREE.Matrix4} 	matrixWorld 	A matrix to transform a vertex into world coordinates.
	 */
	function extendBaseVertices(geometry, matrixWorld)
	{
		var normList = new Array();
		var faceIndices = ['a','b','c'];

		// Map each vertex index to its normal
		for (var i = 0; i < geometry.faces.length; i++)
		{
			var face = geometry.faces[i];

			for (var m = 0; m < faceIndices.length; m++)
			{
				var current = face[faceIndices[m]];
				normList[current] = face.vertexNormals[m].clone();
			}
		}

		for (var i = 0; i < geometry.vertices.length; i++)
		{
			var vertex = geometry.vertices[i];

			if (vertex.y < PRECISION)
			{
				shift = normList[i].clone().setLength(WALL_OFFSET);
				vertex.x += shift.x;
				vertex.z += shift.z;
			}
		}
		geometry.computeFaceNormals();
		geometry.computeVertexNormals();
	}

	/**
	 * Loads resources to be shared and used by all instances of MapView. Resources include textures, material, 
	 * and geometry. This function must be called before any instances are created.
	 * 
	 * @param  {Number} 	size  	The size of the map in units. Equal to the length of the map data.
	 * @param  {Number} 	scale 	The scale of the map.
	 */
	MapView.loadResources = function(size, scale)
	{
		MapView.size = size;
		MapView.scale = scale;
		MapView.mapLength = size * scale;
		MapView.blockHeight = scale * WALL_SCALE;

		// WIRE MATERIAL
		MapView.wireMaterial = new THREE.MeshBasicMaterial({
			color: 0x00bb00,
			wireframe: true,
		});

		// BLOCK GEOMETRY
		MapView.tipBlockGeometry = new TipBlockGeometry(1, WALL_SCALE, 3, 1);
		MapView.cornerBlockGeometry = new CornerBlockGeometry(1, WALL_SCALE , 3, 1);
		MapView.sideBlockGeometry = new SideBlockGeometry(1, WALL_SCALE, 3, 1);

		// BLOCK MATERIAL
		MapView.blockFaceMaterial = new THREE.MeshLambertMaterial({
			side: THREE.BackSide,
			map: THREE.ImageUtils.loadTexture('img/grass_side.jpg'),
		});

		// GRID MATERIALS
		var gridMaterialList = [];
		var gridTextureList = [];
		var materialCount = 10;
 
		gridMaterialList[0] = new THREE.MeshLambertMaterial({
			map: THREE.ImageUtils.loadTexture('img/grass_0.jpg'),
		});

		gridMaterialList[1] = new THREE.MeshLambertMaterial({
			map: THREE.ImageUtils.loadTexture('img/grass_1.jpg'),
		});

		// FLOOR MATERIAL
		var hiddenMaterial = gridMaterialList[0].clone();
		hiddenMaterial.visible = false;
		gridMaterialList.unshift(hiddenMaterial);
		MapView.floorMaterial = new THREE.MeshFaceMaterial(gridMaterialList);

		needsLoad = false;
	};

	MapView.prototype = {

		constructor: MapView,

		/**
		 * Gets a reference to the floor mesh.
		 * 
		 * @return {THREE.Mesh} 	The floor mesh.
		 */
		getFloor: function()
		{
			return this._floorMesh;
		},

		/**
		 * Removes all objects from the scene and disposes geometry.
		 */
		destruct: function()
		{
			this._scene.remove(this._floorMesh);
			this._scene.remove(this._wallMesh);
			
			this._floorMesh.geometry.dispose();

			for (var i = 0; i < this._wallMesh.children.length; i++)
			{
				this._wallMesh.children[i].geometry.dispose();
			}
		},
	};

	return MapView;

})();
