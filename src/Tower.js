/**
 * Class:       Tower
 * Author:      Nathan Hilliard
 * Purpose:     
 * 
 * Date:        August 6, 2014
 */

// Extend from GameObject
Tower.prototype = Object.create(GameObject.prototype);

// Declare our constructor
Tower.prototype.constructor = Tower;

function Tower(row, col)
{
	// Call the parent constructor for THREE.Object3D
	GameObject.call(this, row, col);
}