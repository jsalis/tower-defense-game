/**
 * @class 			HealthBar
 * 
 * @author 			John Salis <jsalis@stetson.edu>
 * @description 	A mesh that represents the health of its parent object.
 */

// Extend from THREE.Object3D
HealthBar.prototype = Object.create(THREE.Object3D.prototype);

// Declare our constructor
HealthBar.prototype.constructor = HealthBar;

HealthBar.scale = 0.3;
HealthBar.geometry = new THREE.PlaneGeometry(HealthBar.scale, HealthBar.scale);
HealthBar.outlineMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });

function HealthBar(material)
{
	// Call the parent constructor for THREE.Object3D
	THREE.Object3D.call(this);

	// Set the horizontal scalar for the health bar
	this.scalar = 0.5;

	// Create health bar
    this.bar = new THREE.Mesh(HealthBar.geometry, material);
    this.add(this.bar);

    // Create health bar outline
    this.outline = new THREE.Mesh(HealthBar.geometry, HealthBar.outlineMaterial);
    this.outline.scale.y += (HealthBar.scale * 2);
    this.outline.position.z = -0.01;
    this.add(this.outline);
}

HealthBar.prototype.run = function()
{
	// Scale the bar based on the health
	this.bar.scale.x = this.scalar * this.parent.health;
	this.outline.scale.x = (this.scalar * this.parent.health) + (HealthBar.scale * 2);

	// Only show the bar when the health is greater then 0
	this.visible = (this.parent.health > 0);
};
