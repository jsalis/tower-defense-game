/**
* Class: CannonTower.js
* Author: John Salis
* Date: November 10th, 2014
*/

CannonTower.prototype = Object.create(Tower.prototype);

CannonTower.prototype.constructor = CannonTower;

// The cost of this tower.
CannonTower.cost = 10;
CannonTower.range = Map.SCALE * 4;

CannonTower.bulletGeometry = new THREE.SphereGeometry(0.15, 16, 16);

function CannonTower(row, col)
{
	Tower.call(this, row, col);

	this.damage = 4;
	this.fireSpeed = 0.25;
	this.fireRate = 90;
	this.cooldown = this.fireRate;

	this.init();
}

// Tower init function to initialize geometry for the Tower
CannonTower.prototype.init = function()
{
	var color = 0x20A6FF;
	var geometry = new THREE.CylinderGeometry(0.3, 0.6, 3, 16);
	var material = new THREE.MeshPhongMaterial({ color: color });
	this.mesh = new THREE.Mesh(geometry, material);

	this.mesh.scale.divideScalar(GameObject.outlineScale);
	this.mesh.scale.x /= GameObject.outlineScale;
	this.mesh.scale.z /= GameObject.outlineScale;
	this.mesh.scale.y = 0.01;
	this.position.y = (this.mesh.geometry.parameters.height * this.mesh.scale.y) / 2;
	this.add(this.mesh);

	// Create outline mesh
	var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
	outline.scale.x *= GameObject.outlineScale;
	outline.scale.z *= GameObject.outlineScale;
	outline.scale.multiplyScalar(GameObject.outlineScale);
	this.mesh.add(outline);

	this.bullets = new THREE.Object3D();
	this.add(this.bullets);
};

CannonTower.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// Apply the growth animation
	if (this.mesh.scale.y < 1)
	{
		this.mesh.scale.y += 0.05;
		this.position.y = (this.mesh.geometry.parameters.height * this.mesh.scale.y) / 2;
	}

	// Update bullets
	var children = this.bullets.children.slice(0);
	for (var i = 0; i < children.length; i++)
	{
		var bullet = children[i];
		bullet.position.sub(bullet.direction);

		if (bullet.position.y < -Map.SCALE)
		{
			this.bullets.remove(bullet);
		}
		else
		{
			// Get a list of the other objects in the scene
			var siblings = this.parent.children;
			for (var i = 0; i < siblings.length; i++)
			{
				if (siblings[i] instanceof Monster)
				{
					var pos = new THREE.Vector3().setFromMatrixPosition(bullet.matrixWorld);
					var dist = pos.distanceTo(siblings[i].position);

					if (dist < Map.SCALE / 4 && siblings[i].health > 0)
					{
						siblings[i].applyDamage(this.damage);
						this.bullets.remove(bullet);
					}
				}
			}
		}
	}

	// If its game over then just return
    if (this.parent.state == GameModel.state.GAME_OVER)
        return;

	if (this.cooldown > 0)
	{
		this.cooldown --;
	}
	else if (this.pew())
	{
		this.cooldown = this.fireRate;
	}
};

CannonTower.prototype.pew = function()
{
	// Get a list of the other objects in the scene
	var siblings = this.parent.children;
	var nearest = null;

	// Find the nearest enemy in range
	for (var i = 0; i < siblings.length; i++)
	{
		if (siblings[i] instanceof Monster)
		{
			var dist = this.position.distanceTo(siblings[i].position);

			if (dist < CannonTower.range && siblings[i].health > 0)
			{
				if (nearest === null || dist < this.position.distanceTo(nearest.position))
				{
					nearest = siblings[i];
				}
			}
		}
	}

	if (nearest !== null)
	{
		var bullet = new THREE.Mesh(CannonTower.bulletGeometry, this.mesh.material);
		bullet.position.y = this.mesh.geometry.parameters.height / 2;

		this.bullets.add(bullet);
		bullet.updateMatrix();
		bullet.updateMatrixWorld();

		// Subtract the current bullet position from the nearest position to get the direction vector
		bullet.direction = new THREE.Vector3().setFromMatrixPosition(bullet.matrixWorld).sub(nearest.position);

		// Calculate the number of steps it will take to reach the target
		var steps = bullet.direction.length() / this.fireSpeed;

		// Estimate the future position of the nearest enemy
		var future = nearest.position.clone().add(nearest.direction.clone().multiplyScalar(steps));

		// Subtract the current bullet position from the future position to improve the direction vector
		bullet.direction = new THREE.Vector3().setFromMatrixPosition(bullet.matrixWorld).sub(future);

		// Set the length of the direction vector to the speed
		bullet.direction.setLength(this.fireSpeed);

		GameModel.audio['pew'].currentTime = 0;
		GameModel.audio['pew'].play();

		return true;
	}
	else
		return false;
};
