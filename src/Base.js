/**
 * Class:       Base
 * Author:      John Salis
 * Purpose:     
 * 
 * Date:        October 28th, 2014
 */

// Inherit the GameObject class
Base.prototype = Object.create(GameObject.prototype);

// Declare our constructor
Base.prototype.constructor = Base;

function Base(row, col, health)
{
	// Call parent constructor.
	GameObject.call(this, row, col);

	// Set the initial spawn rate
	this.health = health;

	this.init();
}

// Initialize the geometry for the Base object
Base.prototype.init = function()
{
	var color = 0x20A6FF;
	var size = Map.SCALE;
	var geometry = new THREE.BoxGeometry(size, size, size);
	var material = new THREE.MeshLambertMaterial({ color: color });
    var mesh = new THREE.Mesh(geometry, material);

    mesh.scale.divideScalar(GameObject.outlineScale);
    mesh.position.y = geometry.parameters.height / 2;
    this.add(mesh);

    // Create light
    var light = new THREE.PointLight(color, 4, 6);
    light.position.y = geometry.parameters.height;
    this.add(light);

    // Create outline mesh
    var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
    outline.position.y = mesh.position.y;
    this.add(outline);

    // Create health bar
    var barMaterial = new THREE.MeshBasicMaterial({ color: color });
    this.healthBar = new HealthBar(barMaterial);
    this.healthBar.position.y = geometry.parameters.height * 2;
	this.add(this.healthBar);
};

Base.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// Update the health bar
	this.healthBar.run();

	// Make the health bar look at the camera
	this.healthBar.lookAt(this.parent.camera.position);

	// Get a list of the other objects in the scene
	var siblings = this.parent.children;

	for (var i = 0; i < siblings.length; i++)
	{
		if (siblings[i] instanceof Monster)
		{
			if (this.position.distanceTo(siblings[i].position) < Map.SCALE / 2)
			{
				this.health -= siblings[i].damage;
				this.health = Math.max(this.health, 0);
				this.parent.remove(siblings[i]);
				GameModel.audio['damage'].play();
			}
		}
	}
};
