/**
 * Class:       Heap
 * Author:      John Salis
 * Purpose:     Data structure that keeps track of either 
 *              the min or max value
 * 
 * Date:        January 25, 2014
 */

Heap.type = {
	MIN: 0,
	MAX: 1
};

Heap.prototype.constructor = Heap;

function Heap(type, size)
{
	this.type = type;
	this.data = new Array(size);
	this.data[0] = 0;
}


Heap.prototype.size = function()
{
	return this.data[0];
}


Heap.prototype.addElement = function(id, val)
{
	this.data[0] ++;
	this.data[this.size()] = { ID: id, value: val };
	this.bubbleUp(this.size());
}


Heap.prototype.setElement = function(id, val)
{
	for (var i = 1; i < this.size(); i++)
	{
		if (this.data[i].ID == id)
		{
			if ((this.data[i].value > val && this.type == Heap.type.MIN) || 
				(this.data[i].value < val && this.type == Heap.type.MAX))
			{
				this.data[i].value = val;
				this.bubbleUp(i);
			} 
			else
			{
				this.data[i].value = val;
				this.bubbleDown(i);
			}
			return true;
		}
	}
	return false;
}


Heap.prototype.bubbleUp = function(i)
{
	var parent;
	var current = i;
	while (current != 1)
	{
		parent = this.getParent(current);
		if (this.data[current].value < this.data[parent].value && this.type == Heap.type.MIN)
		{
			this.swap(current, parent);
		} 
		else if (this.data[current].value > this.data[parent].value && this.type == Heap.type.MAX)
		{
			this.swap(current, parent);
		}
		else return;
		current = parent;
	}
}


Heap.prototype.bubbleDown = function(i)
{
	var left, right, select;
	var current = i;
	while (this.hasChild(current))
	{
		left = this.getLeftChild(current);
		right = this.getRightChild(current);
		if (right == 0)
		{
			select = left;
		} 
		else if (this.type == Heap.type.MIN)
		{
			select = this.min(left, right);
		}
		else if (this.type == Heap.type.MAX)
		{
			select = this.max(left, right);
		}

		if (this.data[current].value > this.data[select].value && this.type == Heap.type.MIN)
		{
			this.swap(current, select);
		} 
		else if (this.data[current].value < this.data[select].value && this.type == Heap.type.MAX)
		{
			this.swap(current, select);
		}
		else return;
		current = select;
	}
}


Heap.prototype.swap = function(x, y)
{
	var temp = this.data[x];
	this.data[x] = this.data[y];
	this.data[y] = temp;
}


Heap.prototype.min = function(x, y)
{
	if (this.data[x].value < this.data[y].value)
	{
		return x;
	}
	else return y;
}


Heap.prototype.max = function(x, y)
{
	if (this.data[x].value > this.data[y].value)
	{
		return x;
	}
	else return y;
}


Heap.prototype.removeRoot = function()
{
	var root = this.data[1];
	this.data[1] = this.data[this.size()];
	this.data[0] --;
	this.bubbleDown(1);
	return root;
}


Heap.prototype.getRoot = function()
{
	return this.data[1];
}


Heap.prototype.getParent = function(i)
{
	return Math.floor(i / 2);
}


Heap.prototype.getLeftChild = function(i)
{
	var c = i * 2;
	if (c > this.size())
	{
		return 0;
	}
	else return c;
}


Heap.prototype.getRightChild = function(i)
{
	var c = (i * 2) + 1;
	if (c > this.size())
	{
		return 0;
	}
	else return c;
}


Heap.prototype.hasChild = function(i)
{
	if (this.getLeftChild(i) == 0 && this.getRightChild(i) == 0)
	{
		return false;
	} 
	else return true;
}


Heap.prototype.hasID = function(id)
{
	for (var i = 1; i <= this.size(); i++)
	{
		if (this.data[i].ID == id) 
		{
			return true;
		}
	}
	return false;
}


Heap.prototype.print = function()
{
	var str = "";
	var max = 2;
	for (var i = 1; i <= this.size(); i++)
	{
		str += this.data[i].ID + " ";
		if (i == max - 1 || i == this.size())
		{
			console.log(str);
			str = "";
			max *= 2;
		}
	}
}

