/**
* Class: ShockwaveTower.js
* Author: Nathan Hilliard
* Date: November 10th, 2014
*/

ShockwaveTower.prototype = Object.create(Tower.prototype);

ShockwaveTower.prototype.constructor = ShockwaveTower;

// The cost of this tower.
ShockwaveTower.cost = 12;
ShockwaveTower.range = Map.SCALE * 2;

function ShockwaveTower(row, col)
{
	Tower.call(this, row, col);
	this.damage = 6;

	this.rate = 200;
	this.cooldown = this.rate;

	this.init();
}

// Tower init function to initialize geometry for the Tower
ShockwaveTower.prototype.init = function()
{
	var color = 0x20A6FF;
	var geometry = new THREE.SphereGeometry(0.6, 16, 16);
	var material = new THREE.MeshPhongMaterial({ color: color });
	this.mesh = new THREE.Mesh(geometry, material);

	this.mesh.scale.divideScalar(GameObject.outlineScale);
	this.mesh.scale.set(0.01, 0.01, 0.01);
	this.add(this.mesh);

	// Create outline mesh
	var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
	outline.scale.x *= GameObject.outlineScale;
	outline.scale.z *= GameObject.outlineScale;
	outline.scale.multiplyScalar(GameObject.outlineScale);
	this.mesh.add(outline);

	var sphereGeometry = new THREE.SphereGeometry(1, 16, 16);
	var sphereMaterial = new THREE.MeshBasicMaterial({ 
		color: color,
		transparent: true,
		opacity: 0
	});

	this.sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
	this.add(this.sphere);
};

ShockwaveTower.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// Apply the growth animation
	if (this.mesh.scale.y < 1)
	{
		this.mesh.scale.addScalar(0.05);
	}

	// Apply attack animation
	if (this.sphere.scale.x < ShockwaveTower.range)
	{
		this.sphere.scale.addScalar(0.05);
		this.sphere.material.opacity -= 0.013;
	}

	// If its game over then just return
    if (this.parent.state == GameModel.state.GAME_OVER)
        return;

	if (this.cooldown > 0)
	{
		this.cooldown --;
	}
	else
	{
		this.cooldown = this.rate;
		this.pew();
	}
};

ShockwaveTower.prototype.pew = function()
{
	this.sphere.scale.set(0.01, 0.01, 0.01);
	this.sphere.material.opacity = 1;

	// Get a list of the other objects in the scene
	var siblings = this.parent.children;

	for (var i = 0; i < siblings.length; i++)
	{
		if (siblings[i] instanceof Monster)
		{
			if (this.position.distanceTo(siblings[i].position) < ShockwaveTower.range && siblings[i].health > 0)
			{
				siblings[i].applyDamage(this.damage);
			}
		}
	}
};
