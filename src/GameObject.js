/**
 * Class:       GameObject
 * Author:      Nathan Hilliard
 * Purpose:     
 * 
 * Date:        October 28th, 2014
 */

// Extend from THREE.Object3D
GameObject.prototype = Object.create(THREE.Object3D.prototype);

// Declare our constructor
GameObject.prototype.constructor = GameObject;

GameObject.outlineMaterial = new THREE.MeshLambertMaterial({ color: 0x000000, side: THREE.BackSide });
GameObject.outlineScale = 1.1;

function GameObject(row, col)
{
	// Call the parent constructor for THREE.Object3D
	THREE.Object3D.call(this);

	// Update the position of this Game Object based on the row and column
	var pos = Map.getCellPosition(Math.round(row), Math.round(col));
	this.position.set(pos.x, 0, pos.z);

	// Used to count the number of updates
	this.tick = 0;
}

GameObject.prototype.run = function()
{
	this.tick ++;
};

GameObject.prototype.getCellRowCol = function()
{
	return Map.getCellRowCol(this.position.z, this.position.x);
};

GameObject.prototype.getCellIndex = function()
{
	var pos = Map.getCellRowCol(this.position.z, this.position.x);
	return (pos.row * Map.SIZE) + pos.col;
};
