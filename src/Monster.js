/**
 * Class:       Monster
 * Author:      Tom Wright
 * Purpose:     
 * 
 * Date:        October 31, 2014
 */

// Extend from GameObject
Monster.prototype = Object.create(GameObject.prototype);

// Declare our constructor
Monster.prototype.constructor = Monster;

Monster.faces = [
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_face_1.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_face_2.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_face_3.jpg') })
];

Monster.hurt = [
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_damage_1.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_damage_2.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_damage_3.jpg') })
];

Monster.sides = [
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_side_1.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_side_2.jpg') }),
	new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture('img/enemy_side_3.jpg') })
];

Monster.colors = [
	0xD39A6E,
	0xF0E07C,
	0xB95B57
];

Monster.state = {
	IDLE: 0,
	DAMAGED: 1
};

function Monster(row, col)
{
	// Call parent constructor
	GameObject.call(this, row, col);

	this.size = Map.SCALE / 2;
	this.position.y = this.size / 2;

	this.direction = new THREE.Vector3();
	this.health = 10;
	this.speed = 0.04;
	this.damage = 1;
	this.target = { row: 0, col: 0 };
	this.path = [];

	this.stateLastChanged = 0;

	this.state = Monster.state.IDLE;

	this.stateLength = {
		damaged: 10
	};

	// Choose random enemy skin
	this.skin = Math.round(Math.random() * (Monster.faces.length - 1));

	this.init();
}

// Monster init function to initialize geometry for the Monster
Monster.prototype.init = function()
{
	var color = Monster.colors[this.skin];
	var geometry = new THREE.BoxGeometry(this.size, this.size, this.size);

	// set material for each face of the cube
	var materialList = [
		Monster.sides[this.skin],
		Monster.sides[this.skin],
		Monster.sides[this.skin],
		Monster.sides[this.skin],
		Monster.faces[this.skin],
		Monster.sides[this.skin],
	];

    this.mesh = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materialList));

    this.mesh.scale.divideScalar(GameObject.outlineScale);
    this.mesh.castShadow = true;
    this.add(this.mesh);

    // Create outline mesh
    var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
    outline.scale.multiplyScalar(GameObject.outlineScale);
    this.mesh.add(outline);

    // Create health bar
    var barMaterial = new THREE.MeshBasicMaterial({ color: color });
    this.healthBar = new HealthBar(barMaterial);
    this.healthBar.position.y = this.size * 2;
	this.add(this.healthBar);
};

Monster.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// Update the health bar
	this.healthBar.run();

	// Make the health bar look at the camera
	this.healthBar.lookAt(this.parent.camera.position);

	// If its game over then just return
    if (this.parent.state == GameModel.state.GAME_OVER)
        return;

	// Apply the death animation if the health is <= 0
	if (this.health <= 0)
	{
		this.mesh.scale.addScalar(-0.1);

		if (this.mesh.scale.x <= 0)
		{
			this.parent.credits ++;
			this.parent.remove(this);

			GameModel.audio['death'].currentTime = 0;
			GameModel.audio['death'].play();	
		}
		return;
	}

	if (this.state == Monster.state.DAMAGED && this.tick > this.stateLastChanged + this.stateLength.damaged)
	{
		this.state = Monster.state.IDLE;
		this.mesh.material.materials[4] = Monster.faces[this.skin];
	}

	if (this.path.length > 0)
	{
		// Get the XZ position from the next node in the path
		var rc = Map.getRowCol(this.path[this.path.length - 1]);
		var pos = Map.getCellPosition(rc.row, rc.col);

		// Create a new vector from the XZ position of the node
		var node = new THREE.Vector3(pos.x, 0, pos.z);

		// Subtract the current position from the node position to get the direction vector
		this.direction = node.clone().sub(this.position).setY(0);

		// Set the length of the direction vector to the speed
		this.direction.setLength(this.speed);

		// Update the position by adding the new direction vector
		this.position.add(this.direction);

		// Make the mesh look in the direction that its moving
		this.mesh.lookAt(new THREE.Vector3(this.direction.x, this.mesh.position.y, this.direction.z));

		// Remove the node from the path if the current distance to the node is close enough
		if (this.position.clone().setY(0).distanceTo(node) < Map.SCALE / 6)
		{
			this.path.pop();
		}
	}
};

/**
 * Updates the path if the specified cell index is on the current path.
 * 
 * @param  {Array}  data       The 2D array of map data.
 * @param  {Number} cellIndex  The index of the cell to search for.
 * @return {Boolean}           False if the path is blocked, true otherwise.
 */
Monster.prototype.updatePath = function(data, cellIndex)
{
	// Check if the specified cell index is on the path.
	for (var i = 0; i < this.path.length; i++)
	{
		// If so, the path must be updated.
		if (this.path[i] == cellIndex)
		{
			var start = this.getCellRowCol();
			var newPath = Generator.findPath(data, start, this.target);

			if (newPath.length > 0)
			{
				this.path = newPath;
				return true;
			}
			else
				return false;
		}
	}
	return true;
};

/**
 * [applyDamage description]
 * 
 * @param  {Number} damage 	The amount of damage to apply.
 */
Monster.prototype.applyDamage = function(damage)
{
	this.health -= damage;
	this.mesh.material.materials[4] = Monster.hurt[this.skin];
	this.state = Monster.state.DAMAGED;
	this.stateLastChanged = this.tick;
};
