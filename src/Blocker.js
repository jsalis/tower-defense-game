/**
 * Class:       Blocker
 * Author:      John Salis
 */

// Inherit the GameObject class
Blocker.prototype = Object.create(GameObject.prototype);

// Declare our constructor
Blocker.prototype.constructor = Blocker;

Blocker.cost = 5;

function Blocker(row, col)
{
	// Call parent constructor
	GameObject.call(this, row, col);

	this.init();
}

// Initialize the geometry for the Blocker object
Blocker.prototype.init = function()
{
	var color = 0x008A7E;
	var size = Map.SCALE;
	var geometry = new THREE.CylinderGeometry(0.6, 0.6, 1, 16);
	var material = new THREE.MeshLambertMaterial({ color: color });
    this.mesh = new THREE.Mesh(geometry, material);

    this.mesh.scale.divideScalar(GameObject.outlineScale);
    this.position.y = (this.mesh.geometry.parameters.height * this.mesh.scale.y) / 2;
    this.mesh.scale.y = 0.01;
    this.add(this.mesh);

    // Create outline mesh
    var outline = new THREE.Mesh(geometry.clone(), GameObject.outlineMaterial);
    outline.scale.x *= GameObject.outlineScale;
    outline.scale.z *= GameObject.outlineScale;
    outline.scale.multiplyScalar(GameObject.outlineScale)
    this.mesh.add(outline);
};

Blocker.prototype.run = function()
{
	// Run parent function
	GameObject.prototype.run.call(this);

	// Apply the growth animation
	if (this.mesh.scale.y < 1)
	{
		this.mesh.scale.y += 0.05;
		this.position.y = (this.mesh.geometry.parameters.height * this.mesh.scale.y) / 2;
	}
};
