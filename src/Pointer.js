/**
 * @class 			Pointer
 * 
 * @author      	John Salis <jsalis@stetson.edu>
 * @description 	
 */

Pointer.prototype.constructor = Pointer;

function Pointer(scene, camera, data)
{
	this.enabled = true;

	var selection = {
		row : null,
		col : null,
		width : 1,
		height : 1,
		hasCollision : false,
	};

	var onClickEvents = [];
	var onRightClickEvents = [];

	var targets = [];

	var mouse = new THREE.Vector2();
	var projector = new THREE.Projector();
	var raycaster = new THREE.Raycaster();

	var scale = Map.SCALE;
	var geometry = new THREE.PlaneGeometry(1, 1);
	var material = new THREE.MeshBasicMaterial({
		color: 0x000088,
		transparent: true,
		opacity: 0.25,
	});

	// Create mesh for the highlight.
	var highlight = new THREE.Mesh(geometry, material);
	highlight.rotation.x = Math.PI / -2;

	// Create mesh for the range.
	var segments = 32;
	var step = (2 * Math.PI) / segments;
	var geometry = new THREE.Geometry();

	for (var s = 0, i = 0; i <= segments; i++)
	{
		geometry.vertices.push(new THREE.Vector3(Math.cos(s), 0, Math.sin(s)));
		s += step;
	}

	var material = new THREE.LineBasicMaterial({ color: 0x00BBAB, linewidth: 1.5 });
    var range = new THREE.Line(geometry, material);

    // Create container for the highlight and range and addd it to the scene.
    var container = new THREE.Object3D();
    container.add(highlight);
    container.add(range);
	scene.add(container);

	document.addEventListener('mousemove', onMouseMove, false);
	document.addEventListener('mouseup', onMouseClick, false);

	function onMouseMove(event)
	{
		event.preventDefault();

		mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
		mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
	}

	function onMouseClick(event)
	{
		event.preventDefault();

		// Check to see if it was a right click
		var isRightClick = false;

		if ("which" in event)
			isRightClick = (event.which == 3);
		else if ("button" in event)
			isRightClick = (event.button == 2);

		// Check to see if there's a collision with the map.
		var collision = !selection.hasCollision && container.visible;

		if (!isRightClick)
		{
			for (var i = 0; i < onClickEvents.length; i++)
				if (onClickEvents[i].collision === null)
					onClickEvents[i].event.call();
				else if (collision && onClickEvents[i].collision)
					onClickEvents[i].event.call();
				else if (!collision && !onClickEvents[i].collision)
					onClickEvents[i].event.call();
		}
		else
		{
			for (var i = 0; i < onRightClickEvents.length; i++)
				if (onRightClickEvents[i].collision === null)
					onRightClickEvents[i].event.call();
				else if (collision && onRightClickEvents[i].collision)
					onRightClickEvents[i].event.call();
				else if (!collision && !onRightClickEvents[i].collision)
					onRightClickEvents[i].event.call();
		}
	}

	function checkCollision()
	{
		for (var i = 0; i < selection.width; i++)
		{
			for (var j = 0; j < selection.height; j++)
			{
				var row = selection.row + j;
				var col = selection.col + i;

				if (data[row][col] != Map.FLOOR)
				{
					return true;
				}
			}
		}
		return false;
	}

	function clearSelection()
	{
		selection.row = null;
		selection.col = null;
		container.visible = false;
	}

	this.addOnClickEvent = function(func, onRightClick, onCollision)
	{
		// Default value for the onRightClick & onCollision parameters
		var onRightClick = (typeof onRightClick !== 'undefined') ? onRightClick : false;
		var onCollision = (typeof onCollision !== 'undefined') ? onCollision : null;

		// Construct an object for the onClick event.
		var e = { event: func, collision: onCollision };

		// Add it to the necessary array.
		if (!onRightClick)
			onClickEvents.push(e);
		else
			onRightClickEvents.push(e);
	};

	this.setRange = function(r)
	{
		range.scale.x = r;
		range.scale.z = r;
	};

	this.setRangeVisible = function(v)
	{
		range.visible = v;
	};

	this.setSize = function(width, height)
	{
		selection.width = width;
		selection.height = height;
	};

	this.getSelection = function()
	{
		return selection;
	};

	this.addTarget = function(obj)
	{
		targets.push(obj);
	};

	this.removeTarget = function(obj)
	{
		var index = targets.indexOf(obj);
		if (index != -1)
		{
			targets.splice(index, 1);
		}
	};

    this.run = function()
	{
		if (!this.enabled)
		{
			clearSelection();
			return;
		}

		var vector = new THREE.Vector3(mouse.x, mouse.y, 1);
		projector.unprojectVector(vector, camera);
		raycaster.set(camera.position, vector.sub(camera.position).normalize());

		var intersects = raycaster.intersectObjects(targets);

		if (intersects.length > 0)
		{
			var point = intersects[0].point;
			var cell = Map.getCellRowCol(point.z, point.x);
			var cellPosition = Map.getCellPosition(cell.row, cell.col);

			container.position.z = cellPosition.z + (selection.height / 2 * scale) - (scale / 2);
			container.position.x = cellPosition.x + (selection.width / 2 * scale) - (scale / 2);

			highlight.scale.x = selection.width * scale;
			highlight.scale.y = selection.height * scale;

			selection.row = cell.row;
			selection.col = cell.col;

			var cellValue = data[cell.row][cell.col];
			container.visible = (cellValue == Map.FLOOR);

			selection.hasCollision = checkCollision();
			highlight.material.color.setHex((selection.hasCollision) ? 0xDD0000 : 0x83F8DD);
		}
		else
		{
			clearSelection();
		}
	};
}
